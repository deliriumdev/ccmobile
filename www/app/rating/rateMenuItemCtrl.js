(function () {
    'use strict';

    angular
        .module('rateMeal')
        .config(addRoute)
        .controller('RateMenuItemCtrl', menuDetailCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.rate-items', {
                cache: false,
                url: '/rate/items',
                views: {
                    'tab-rate-meal': {
                        templateUrl: 'app/rating/templates/rate-menu-details.html',
                        controller: 'RateMenuItemCtrl',
                        resolve :{
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    menuDetailCtrl.$inject = ['$scope','UserManager','FormManager',
        'MenuService','studentData','$ionicScrollDelegate','alert','$state','$rootScope'];

    /* @ngInject */
    function menuDetailCtrl($scope,UserManager, FormManager, MenuService,studentData,$ionicScrollDelegate,alert,$state,$rootScope) {
        /* jshint validthis: true */
        //var vm = this;

        var data = MenuService.getMenu();
        var menuItem = {};
        menuItem[1] = data.id; //menu id
        menuItem[4] = studentData.name; //student name
        menuItem[5] = studentData.school; //student school
        menuItem[6] = studentData.house; // student house
        menuItem[99] = studentData.userId; // student id

        $scope.rateFunction = function(rating,item) {
            console.log("Rating selected: " + rating + " Item Name:"+item);
        };

        $scope.submitMealRating = function(itemRating,itemName,comment) {
            if(itemRating == -1){
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('error',null,'Please rate Item first!');
            }
            else
            {
                var rating = {};
                rating['input_1'] = data.id; //menu id
                rating['input_2'] = itemRating;//rating
                rating['input_3'] = comment;//comment
                rating['input_4'] = studentData.name; //student name
                rating['input_5'] = studentData.school; //student school
                rating['input_6'] = studentData.house; // student house
                rating['input_7'] = itemName; //student
                rating['student_id'] = studentData.userId;

                var requestData = {'input_values': rating};
                UserManager.submitMealRating(requestData).then(function(data){
                    if(!data.response.is_valid){
                        var serverResponse = data.response.validation_messages;
                        var errorMessage = '';
                        for (var fieldName in serverResponse) {
                            var message = serverResponse[fieldName];
                            errorMessage = errorMessage + message;
                        }
                        $ionicScrollDelegate.scrollTop();
                        alert.createAlert('error',null,errorMessage);
                    }
                    else if (data.response.is_valid){
                        $ionicScrollDelegate.scrollTop();
                        alert.createAlert('success',null,'Rating submitted successfully!');
                        $state.go($state.current,{},{reload: true});
                    }
                },function (err){
                    $ionicScrollDelegate.scrollTop();
                    alert.createAlert('error',null,err);
                });
            }
        };

        if (data) {
            $scope.typeArray = [false,false,false];// gluten free,vegetarian,dairy free
            $scope.foodArray = [];
            $scope.id = data.id;
            $scope.day = data[2];
            $scope.mealDate = MenuService.getWeekTitle();
            $scope.mealtime = data[3];
            $scope.chefEmail = data[6];

            $scope.school = data[7];
            $scope.house = data[8];
            $scope.chef = data[5];

            var prepareFoodItem = function(itemId,typeId,data,menuItem){
                var foodObject = {};
                foodObject['id'] = itemId;
                foodObject['item'] = data[itemId];
                foodObject['rating'] = -1;
                foodObject['isReadonly'] = false;
                foodObject['comment'];

                var ratingSuccess = function (data){
                    if (data.response.total_count > 0) {
                        foodObject['rating'] = data.response.entries[0][2];
                        foodObject['isReadonly'] = true;
                    }
                };
                var ratingError = function (reason){
                    console.log(reason);
                };

                menuItem[7] = data[itemId]; // item id
                FormManager.getMenuItemRating(menuItem).then(ratingSuccess,ratingError);

                return foodObject;
            };


            if(data[10]){
                var Entree = prepareFoodItem(10,14,data,menuItem);
                $scope.foodArray.push(Entree);
            }
            if(data[16]){
                var sideDish = prepareFoodItem(16,18,data,menuItem);
                $scope.foodArray.push(sideDish);
            }
            if(data[19]){
                var soup = prepareFoodItem(19,21,data,menuItem);
                $scope.foodArray.push(soup);
            }
            if(data[25]){
                var vegetable = prepareFoodItem(25,27,data,menuItem);
                $scope.foodArray.push(vegetable);
            }
            if(data[28]){
                var starch = prepareFoodItem(28,30,data,menuItem);
                $scope.foodArray.push(starch);
            }
            if(data[22]){
                var additionalItem = prepareFoodItem(22,24,data,menuItem);
                $scope.foodArray.push(additionalItem);
            }
            if(data[31]){
                var dessertItem = prepareFoodItem(31,33,data,menuItem);
                $scope.foodArray.push(dessertItem);
            }

        }

    }
})();
