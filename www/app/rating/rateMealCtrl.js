(function () {
    'use strict';

    angular
        .module('rateMeal')
        .config(addRoute)
        .controller('RateMealCtrl', rateMealCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
            $stateProvider
                .state('tab.rate-meal', {
                    cache: false,
                    url: '/meal',
                    views: {
                        'tab-rate-meal': {
                            templateUrl: 'app/rating/templates/tab-rate-meal.html',
                            controller: 'RateMealCtrl',
                            resolve :{
                                studentData: function(getOrCreateUserInformationService){
                                    return getOrCreateUserInformationService.getOrCreateUserInformation();

                                },
                                menu: function(MenuService){
                                    return MenuService.getMenu();

                                }
                            }
                        }
                    }
                });
    }


    rateMealCtrl.$inject = ['$scope','FormManager','$location','UserManager','studentData','menu'];

    /* @ngInject */
    function rateMealCtrl($scope,FormManager,$location,UserManager,studentData,menu) {
        /* jshint validthis: true */
        //var vm = this;

        console.log(studentData);
        console.log(menu);
        $scope.rating = {};

        FormManager.getRatingForm().then(function(data){
                console.log(data.response.fields);
                var result = data.response.fields;

                var form = {};
                //form.menuIdValue = menu.id;
                $scope.rating[result[1].id]=studentData.name;
                $scope.rating[result[2].id]=studentData.school;
                $scope.rating[result[3].id]=studentData.house;
                $scope.rating[result[0].id]=menu.id;
                form.itemName = result[4].id;
                form.commentId = result[6].id;
                $scope.form =form;
            },function (err){
                alert(err);
            }
        );

        $scope.rating[2] = -1;

        $scope.rateMeal = function() {
            console.log('in rateMeal feedback submission');

            var requestData = {'rating': $scope.rating};
            console.log(requestData);

            UserManager.submitMealRating(requestData).then(function(data){
                $location.path('tab/menu/detail');
            },function (err){
                alert(err);
            });
        };
    }
})();