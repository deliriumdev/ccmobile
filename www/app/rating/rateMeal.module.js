(function () {
    'use strict';

    angular
        .module('rateMeal', [
            'customDirective',
            'starter.services',
            'starter.utils',
            'common']);
})();