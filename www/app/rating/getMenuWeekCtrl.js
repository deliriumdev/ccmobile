(function () {
    'use strict';

    angular
        .module('rateMeal')
        .config(addRoute)
        .controller('GetRateWeekCtrl', getMenuCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.rate-week', {
                cache: false,
                url: '/rate/week',
                views: {
                    'tab-rate-meal': {
                        templateUrl: 'app/rating/templates/rate-get-menu-week.html',
                        controller: 'GetRateWeekCtrl',
                        resolve:{
                            weekOptions:function(FormManager){
                                return FormManager.getMenuWeekOptions();
                            }
                        }
                    }
                }
            });
    }

    getMenuCtrl.$inject = ['$scope','$location','FormManager','MenuService','weekOptions'];

    /* @ngInject */
    function getMenuCtrl($scope, $location, FormManager, MenuService, weekOptions) {
        /* jshint validthis: true */
        //var vm = this;
        $scope.dayOption = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

        var arr =[];
        if(weekOptions){
            var template1 = {};
            template1.text =  'This Week';
            template1.value = weekOptions[0];
            arr.push(template1);
            //var template = {};
            //template.text = 'Next Week';
            //template.value = weekOptions[1];
            //arr.push(template);
        }
        $scope.weekOptions = arr;
        //var successMenuForm = function(data){
        //    var form = [];
        //    $scope.form = data.response;
        //    var result = data.response.fields;
        //
        //    for(var i=0; i<result.length;i++){
        //        if(result[i].id == 2){
        //            result[i].label = 'Select Day';
        //            form.push(result[i]);
        //        }
        //        if(result[i].id == 34){
        //            result[i].label = 'Select Week';
        //            result[i].choices = arr;
        //            form.push(result[i]);
        //        }
        //    }
        //
        //    $scope.form.fields =form;
        //
        //};
        //var errorFunction = function (err){
        //    alert(err);
        //};
        //FormManager.getMenuForm().then(successMenuForm,errorFunction);

        $scope.menu = {};
        $scope.getMealtime = function(menu){

            if(menu[34]==arr[0].value){
                MenuService.setWeekTitle('This Week');
            }
            else{
                MenuService.setWeekTitle('Next Week');
            }
            MenuService.setMenuFilters(menu);
            $location.path('tab/rate/mealtime');
        }
    }
})();
