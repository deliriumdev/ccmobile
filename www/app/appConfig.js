var app = angular.module('starter.config', []);
app.constant('ENVIRONMENT',{'staging':1,'production':2})
app.constant('CLOUDINARY_CONFIGS',{
	'UPLOAD_PRESET':'159738698346187',
	'API_URL':'https://api.cloudinary.com/v1_1/delirium-creative-group'
})
app.constant('REQUEST',{'authenticated':1,'non_authenticated':2,'media_upload':3})
app.constant('REQUEST_KEYS',{'secretKey':'490d56eb9362ac75be477fe647ac306f64f7ebab50624b23cc916a6ff2362f22'});
//Will add configration related code here. mostly returning the URLs based on the environemt and later on more environment specific changes may come
app.provider('config',function(ENVIRONMENT){
	var environment;
	var staging_endpoint = "https://mycampuscooks.com/gravityformsapi/";
	var production_endpoint = "https://mycampuscooks.com/gravityformsapi/";
	//var production_endpoint = "/gravityformsapi/";
	var cc_endpoint = "https://mycampuscooks.com/ccapi/";
	var wp_endpoint = "https://www.mycampuscooks.com/";
	return {
		setEnvironment: function(env)
		{
			environment = env
		},
		getBaseURL: function()
		{
			if (environment == ENVIRONMENT.staging)
			{
				return staging_endpoint;
			}
			else 
			{
				return production_endpoint;
			}
		},
		$get: function () {
			return {
				environment: environment,
				base_url : this.getBaseURL(),
                cc_base_url: cc_endpoint,
				wp_base_url: wp_endpoint
			}
		}
	}
});
