(function () {
    'use strict';

    angular
        .module('community', ['starter.services','common','customDirective']);
})();