(function () {
    'use strict';

    angular
        .module('community')
        .config(addRoute)
        .controller('MealSuggestionCtrl', communityCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.meal-suggestion', {
                url: '/community/suggestion',
                views: {
                    'tab-community': {
                        templateUrl: 'app/community/templates/meal-suggestions-form.html',
                        controller: 'MealSuggestionCtrl as vm',
                        resolve: {
                            feedBackForm:function(FormManager){
                                return FormManager.getFeedBackForm();
                            },
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    communityCtrl.$inject = ['feedBackForm','UserManager','studentData','alert'];

    /* @ngInject */
    function communityCtrl(feedBackForm,UserManager,studentData,alert) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'Submit Feedback';
        vm.form =feedBackForm;
        vm.feedback ={};
        vm.feedback[vm.form.studentName] = studentData.name;

        activate();

        ////////////////

        function activate() {

            vm.submitFeedback = function(community) {
                community[vm.form.studentName] = studentData.name;
                var requestData = {'community': community};
                UserManager.submitCommunityFeedback(requestData).then(onSuccessFeedback,onError);
            };

            function onSuccessFeedback(data){
                if(data.status == 201){
                    vm.feedback ={};
                    alert.createAlert('success',null,"Suggestion successfully submitted.");
                }
                else{
                    alert.createAlert('error',null,"Unexpected Server Error!");
                }
            }

            function onError(reason){
                alert.createAlert('error',null,reason);
            }
        }
    }
})();