(function () {
    'use strict';

    angular
        .module('community')
        .config(addRoute)
        .controller('GeneralFeedbackCtrl', communityCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.general-feedback', {
                url: '/community/feedback',
                cache:false,
                views: {
                    'tab-community': {
                        templateUrl: 'app/community/templates/general-feedback-form.html',
                        controller: 'GeneralFeedbackCtrl as vm',
                        resolve: {
                            feedBackForm:function(FormManager){
                                return FormManager.getFeedBackForm();
                            },
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    communityCtrl.$inject = ['feedBackForm','UserManager','studentData','alert','$state'];

    /* @ngInject */
    function communityCtrl(feedBackForm,UserManager,studentData,alert,$state) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'Submit Feedback';
        vm.form =feedBackForm;
        vm.feedback ='';

        activate();

        ////////////////

        function activate() {

            function onError(reason){
                alert.createAlert('error',null,reason);
            }

            vm.submitFeedback = function(form) {

                function onSuccessFeedback(data){
                    if (data.status == 200){
                        if(!data.response.is_valid){
                            var serverResponse = data.response.validation_messages;
                            for (var fieldName in serverResponse) {
                                var message = serverResponse[fieldName];
                                alert.createAlert('error',null,message);
                            }
                        }
                        else if (data.response.is_valid){
                            alert.createAlert('success',null,"Feedback successfully submitted.");
                            $state.go($state.current, {}, {reload: true});
                        }
                    }
                    else {
                        alert.createAlert('error',null,"Unexpected Server Error!");
                    }
                }

                var community = {};
                community[vm.form.studentName] = studentData.name;
                community[vm.form.feedback] = vm.feedback;
                community[vm.form.studentId] = studentData.userId;
                var requestData = {'input_values': community};
                if (form.$valid) {
                    UserManager.submitCommunityFeedback(requestData).then(onSuccessFeedback,onError);
                }
            };

        }
    }
})();
