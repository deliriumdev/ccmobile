(function () {
    'use strict';

    angular
        .module('community')
        .config(addRoute)
        .controller('ThreadDetailCtrl', communityCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.thread-detail', {
                url: '/community/thread-detail:id',
                cache:false,
                views: {
                    'tab-community': {
                        templateUrl: 'app/community/templates/thread-messages-detail.html',
                        controller: 'ThreadDetailCtrl as vm',
                        resolve: {
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    communityCtrl.$inject = ['UserManager','studentData','alert','$stateParams','storageHelper','$state'];

    /* @ngInject */
    function communityCtrl(UserManager,studentData,alert,$stateParams,storageHelper,$state) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'Thread Detail';
        vm.form ={
            campus: 1,
            house: 2,
            studentName: 3,
            message: 5,
            parent :6
        };
        vm.feedback ={};
        vm.message;

        activate();

        ////////////////

        function activate() {
            var onSuccessThreads = function(data){
                if(data.status==200){
                    vm.threads = data.response.entries;
                }
                else{
                    alert.createAlert('warning',null,"threads request error.");
                }
            };
            var requestData = {};
            requestData[vm.form.parent] = $stateParams.id;
            vm.subject = storageHelper.getSubject();

            UserManager.getCommunityThreads(requestData).then(onSuccessThreads,onError);

            vm.createThread = function(community) {
                console.log(studentData);
                community[vm.form.campus] = studentData.school;
                community[vm.form.house] = studentData.house;
                community[vm.form.studentName] = studentData.name;
                community[vm.form.parent] = $stateParams.id;
                var requestData = {'community': community};
                UserManager.sendCommunityMessage(requestData).then(onSuccessFeedback,onError);
            };


            function onSuccessFeedback(data){
                if(data.status == 201){
                    $state.go($state.current,{id:$stateParams.id},{reload: true});
                }
                else{
                    alert.createAlert('error',null,"Unexpected Server Error!");
                }
            }

            function onError(reason){
                alert.createAlert('error',null,reason);
            }
        }
    }
})();