(function () {
    'use strict';

    angular
        .module('community')
        .config(addRoute)
        .controller('communityCtrl', communityCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.community', {
                url: '/community',
                views: {
                    'tab-community': {
                        templateUrl: 'app/community/templates/community-type-selection.html',
                        controller: 'communityCtrl as vm'
                    }
                }
            });
    }

    communityCtrl.$inject = [];

    /* @ngInject */
    function communityCtrl() {
        /* jshint validthis: true */
        var vm = this;
        vm.activate = activate;

        activate();

        ////////////////

        function activate() {

        }
    }
})();