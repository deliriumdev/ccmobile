(function () {
    'use strict';

    angular
        .module('community')
        .config(addRoute)
        .controller('MessageBoardCtrl', communityCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.message-board', {
                url: '/community/board',
                cache:false,
                views: {
                    'tab-community': {
                        templateUrl: 'app/community/templates/message-board.html',
                        controller: 'MessageBoardCtrl as vm',
                        resolve: {
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    communityCtrl.$inject = ['UserManager','studentData','alert','storageHelper','$state'];

    /* @ngInject */
    function communityCtrl(UserManager,studentData,alert,storageHelper,$state) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'community Board';
        vm.form ={
            campus: 1,
            house: 2,
            studentName: 3,
            subject:4,
            message: 5,
            parent: 6
        };
        vm.feedback ={};

        activate();

        ////////////////

        function activate() {
            var onSuccessThreads = function(data){
                if(data.status==200){
                    vm.threads = data.response.entries;
                }
                else{
                    alert.createAlert('warning',null,"threads request error.");
                }
            };
            var requestData = {};
            requestData[vm.form.campus] = studentData.school;
            requestData[vm.form.house] = studentData.house;
//            requestData[vm.form.studentName] = studentData.name;
            requestData[vm.form.parent] = -1;

            UserManager.getCommunityThreads(requestData).then(onSuccessThreads,onError);

            vm.createThread = function(community) {
                console.log(studentData);
                community[vm.form.campus] = studentData.school;
                community[vm.form.house] = studentData.house;
                community[vm.form.studentName] = studentData.name;
                community[vm.form.parent] = -1;
                var requestData = {'community': community};
                UserManager.sendCommunityMessage(requestData).then(onSuccessFeedback,onError);
            };

            vm.openThread = function(item){
                storageHelper.setSubject(item[4]);
                $state.go('tab.thread-detail',{id:item.id})
            };
            function onSuccessComment(data){
                if(data.status == 201){
                    $state.go($state.current, {}, {reload: true});
                }
                else{
                    alert.createAlert('error',null,"Unexpected Server Error!");
                }
            }
            function onSuccessFeedback(data){
                if(data.status == 201){
                    var comment = {};
                    comment[vm.form.campus] = studentData.school;
                    comment[vm.form.house] = studentData.house;
                    comment[vm.form.studentName] = studentData.name;
                    comment[vm.form.message] = vm.message;
                    comment[vm.form.parent] = data.response[0];
                    var requestData = {'community': comment};
                    UserManager.sendCommunityMessage(requestData).then(onSuccessComment,onError);
                }
                else{
                    alert.createAlert('error',null,"Unexpected Server Error!");
                }
            }

            function onError(reason){
                alert.createAlert('error',null,reason);
            }
        }
    }
})();