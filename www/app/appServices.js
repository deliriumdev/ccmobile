var app = angular.module('starter.services', ['starter.utils', 'ab-base64']);

app.factory('StorageManager', ['$ionicLoading', function ($ionicLoading) {
    return {
        setValue: function (key, value) {
            localStorage[key] = value;
        },
        getValue: function (key, defaultValue) {
            return localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            try {
                return JSON.parse(localStorage[key]);
            }
            catch (err) {
                return err;
            }
        },
        deleteByKey: function (key) {
            localStorage.removeItem(key)
        }
    }
}]);

app.service('DatabaseManager', function ($http, $q, $ionicLoading, config, REQUEST) {
    var lib;
    return {
        initDB: function () {
            var deferred = $q.defer();
            lib = new localStorageDB("cook", localStorage);
            if (lib.isNew()) {
                lib.createTable("student", ["userId", "name", "firstName", "lastName", "house", "school", "offCampus"]);
                lib.commit();
            }
            else {
                if (!(lib.columnExists("student", "offCampus"))) {
                    lib.alterTable("student", "offCampus", "");
                    lib.commit();
                }
                if (!(lib.columnExists("student", "firstName"))) {
                    lib.alterTable("student", "firstName", "");
                    lib.commit();
                }
                if (!(lib.columnExists("student", "lastName"))) {
                    lib.alterTable("student", "lastName", "");
                    lib.commit();
                }
            }
            deferred.resolve();
            return deferred.promise;
        },

        inserStudent: function (student) {
            lib.insertOrUpdate("student", { userId: student.id }, { userId: student.id, name: student.name, firstName: student.firstName, lastName: student.lastName, school: student.school, house: student.house, offCampus: student.offCampus });
            lib.commit();
        },
        inserOrUpdateStudentId: function (student) {
            lib.insertOrUpdate("student", { userId: student.id }, { userId: student.id });
            lib.commit();
        },
        updateStudentInfo: function (student) {
            lib.insertOrUpdate("student", { userId: student.id }, { userId: student.id, name: student.name, firstName: student.firstName, lastName: student.lastName, offCampus: student.offCampus });
            lib.commit();
        },
        getAllStudent: function () {
            return lib.query("student")
        },
        getStudentWithId: function (studentid) {
            return lib.query("student", { userId: studentid });
        },
        removeStudent: function (studentid) {
            // delete sudent with given id
            var deleteCount = lib.deleteRows("student", { userId: studentid });
            lib.commit(); // commit the deletions to localStorage
            return deleteCount
        }
    }
});

app.factory('RequestManager', function ($http, $q, $ionicLoading, config, REQUEST, AnalyticsFactory, UserInformationService) {
    return {
        get: function (url) {
            //request_headers = UtilsFactory.getRequestHeaders(type);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "get",
                url: config.base_url + url,
                headers: { "Content-Type": "application/json" }
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        getWithParams: function (url, params, urlType) {
            //request_headers = UtilsFactory.getRequestHeaders(type);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "get",
                // url: config.base_url + url,
                url: ('string' === typeof (urlType) && 'CC' === urlType ? config.cc_base_url : config.base_url) + url,
                headers: { "Content-Type": "application/json" },
                params: { "search": params }
                //                paramSerializer: '$httpParamSerializerJQLike'
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        wp_get: function (url) {
            //request_headers = UtilsFactory.getRequestHeaders(type);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "get",
                url: config.wp_base_url + url
                //headers: request_headers
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        wp_post: function (url, data) {
            request_header = { 'Content-Type': 'application/x-www-form-urlencoded' };
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "post",
                url: config.wp_base_url + url,
                headers: request_header,
                data: data
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        post_without_header: function (url, data) {
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "post",
                url: config.wp_base_url + url,
                data: data,
                headers: { "Content-Type": "application/x-www-form-urlencoded" }
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        postFormData: function (url, data) {
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "post",
                url: config.wp_base_url + url,
                data: $.param(data),
                headers: { "Content-Type": "application/x-www-form-urlencoded" }
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        post: function (url, data, urlType) {
            //request_headers = UtilsFactory.getRequestHeaders(type);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "post",
                url: ('string' === typeof (urlType) && 'CC' === urlType ? config.cc_base_url : config.base_url) + url,
                //headers: request_headers,
                data: data
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (data) {
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(data);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        put: function (url, data) {
            //request_headers = UtilsFactory.getRequestHeaders(REQUEST.authenticated);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "put",
                url: config.base_url + url,
                //headers: request_headers,
                data: data
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (data) {
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(data);
                $ionicLoading.hide();
            });
            return deferred.promise;
        }, putWithParams: function (url) {
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "put",
                url: config.wp_base_url + url,
                headers: { "Content-Type": "application/json" }
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (err) {
                console.log(err);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(err);
                $ionicLoading.hide();
            });
            return deferred.promise;
        },
        delete: function (url) {
            //request_headers = UtilsFactory.getRequestHeaders(REQUEST.authenticated);
            $ionicLoading.show();
            var deferred = $q.defer();
            $http({
                method: "delete",
                url: config.base_url + url
                //headers: request_headers
            }).success(function (data) {
                deferred.resolve(data);
                $ionicLoading.hide();
            }).error(function (data) {
                console.log(data);
                AnalyticsFactory.userError(UserInformationService.getUserId(), url, err);
                deferred.reject(data);
                $ionicLoading.hide();
            });
            return deferred.promise;
        }
    }
});

app.service('UserManager', function (RequestManager, $rootScope, REQUEST, StorageManager, SignatureFactory, SearchFiltersFactory, UserInformationService) {
    return {
        getUserNotifications: function (appVersion) {
            userId = UserInformationService.getUserId()
            return RequestManager.wp_get('ccapi/notices/' + userId + '/' + appVersion)
            //return RequestManager.wp_get('ccapi/notices/'+userId)
        },
        getUserLatePlates: function () {
            userId = UserInformationService.getUserId()
            return RequestManager.wp_get('ccapi/data/lateplate/' + userId)
        },
        removeLatePlate: function (lateplate_id) {
            var url = "ccapi/lateplate/remove/" + lateplate_id;
            userId = UserInformationService.getUserId()
            data = {
                token: SignatureFactory.appendToken(userId),
                id: userId
            };
            return RequestManager.postFormData(url, data);
        },
        getUser: function () {
            return StorageManager.getObject('user')
        },
        updateUser: function (userData) {
            //returns the promise
            return RequestManager.put('api/merchants.json', { "merchant": userData })
        },
        getGroups: function (userData) {
            return RequestManager.wp_post('wp-admin/admin-ajax.php', userData)
        },
        runWPAction: function (userData) {
            return RequestManager.wp_post('wp-admin/admin-ajax.php', userData)
        },
        is_lateplateEnable: function (userData) {
            return RequestManager.postFormData('wp-admin/admin-ajax.php?action=late_plate_bool', userData)
        },
        getAuthenticationCookie: function (userData) {
            return RequestManager.wp_post('api/user/generate_auth_cookie/', userData)
        },
        getUserMeta: function (userData) {
            return RequestManager.wp_post('api/user/get_user_meta/', userData)
        },
        forgetPassword: function (userData) {
            return RequestManager.wp_get('api/user/retrieve_password/?' + userData)
        },
        authenticateUser: function (userData) {
            //encoded_string = base64.encode(userData)
            request_header = { 'Authorization': 'Basic ' + userData };
            return RequestManager.getWithHeader('wp-json/users/me', request_header)
        },
        registerUser: function (userData) {
            var url = SignatureFactory.appendSignature('forms/11/submissions', "POST", 3600);
            return RequestManager.post(url, userData);
        },
        updateUserProfile: function (userData) {
            var url = 'api/user/update_user_meta_vars';
            var argumentedUrl = url + '/?cookie=' + userData.cookie + '&first_name=' + userData.first_name + '&last_name=' + userData.last_name + '&live_in_house=' + userData.live_in_house;
            return RequestManager.putWithParams(argumentedUrl);
        },
        getStudentInformation: function (studentData) {
            var url = SignatureFactory.appendSignature('forms/11/entries', "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, studentData);
            return RequestManager.getWithParams(url, argumentedUrl);
        },
        getStudentMenu: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/7/entries', "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, menuData);
            return RequestManager.getWithParams(url, argumentedUrl);
        },
        submitMealRating: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/ratemeal', "POST", 3600);
            return RequestManager.post(url, menuData, 'CC')

        },
        // @note this is not called anywhere can probably be depricated
        submitLatePlateRequest: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/lateplate', "POST", 3600);
            return RequestManager.post(url, menuData, 'CC')

        },
        requestHouseData: function (house) {
            // var url = SignatureFactory.appendSignature('house/search',"POST",3600);
            return RequestManager.post('house/search/?house=' + house, '', 'CC');
        },
        requestLatePlate: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/lateplate', "POST", 3600);
            return RequestManager.post(url, menuData, 'CC')

        },
        requestAttending: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/attending', "POST", 3600);
            return RequestManager.post(url, menuData, 'CC');
        },
        submitCommunityFeedback: function (communityData) {
            var url = SignatureFactory.appendSignature('forms/feedback', "POST", 3600);
            return RequestManager.post(url, communityData, 'CC')
        },
        getMenuRating: function (menuData) {
            var url = SignatureFactory.appendSignature('forms/10/entries', "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, menuData);
            return RequestManager.getWithParams(url, argumentedUrl)

        },
        getCommunityThreads: function (communityData) {
            var url = SignatureFactory.appendSignature('forms/22/entries', "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, communityData);
            return RequestManager.getWithParams(url, argumentedUrl);
        },
        getCommunityThread: function (communityData) {
            var url = SignatureFactory.appendSignature('forms/22/entries', "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, communityData);
            return RequestManager.getWithParams(url, argumentedUrl);
        },
        sendCommunityMessage: function (communityData) {
            var url = SignatureFactory.appendSignature('forms/22/entries', "POST", 3600);
            return RequestManager.post(url, communityData)
        },
        pinVerification: function (userData) {
            return RequestManager.post_without_header('wp-admin/admin-ajax.php?action=pin_to_house', userData);
        },
        logout: function () {
            return RequestManager.delete('api/merchants/sign_out.json')
        }
    }
});

app.service('FormManager', function (RequestManager, REQUEST, StorageManager, SignatureFactory, SearchFiltersFactory) {
    return {
        getRegistrationForm: function () {
            var url = SignatureFactory.appendSignature('forms/11', "GET", 3600);
            return RequestManager.get(url, 'GET')

        },
        getMenuForm: function () {
            var url = SignatureFactory.appendSignature('forms/7', "GET", 3600);
            return RequestManager.get(url, 'GET')

        },
        getMenuWeekOptions: function () {
            var url = 'wp-admin/admin-ajax.php?action=get_mondays';
            return RequestManager.wp_get(url);
        },
        getRatingForm: function () {
            var url = SignatureFactory.appendSignature('forms/10', "GET", 3600);
            return RequestManager.get(url, 'GET')

        },
        getLatePlateForm: function () {
            var url = SignatureFactory.appendSignature('forms/9', "GET", 3600);
            return RequestManager.get(url, 'GET')
        },
        getFeedBackForm: function () {
            //var deferred = $q.defer();
            var form = {};
            form.studentName = 'input_1';//1
            form.feedback = 'input_2'; //2
            form.studentId = 'student_id';
            //deferred.resolve(studentData)
            //return deferred.promise
            return form
        },
        getMenuItemRating: function (menuData) {
            //var url = SignatureFactory.appendSignature('forms/10/entries',"GET",3600);
            // pass student id in url
            var url = SignatureFactory.appendSignature('data/ratemeal/' + menuData[99], "GET", 3600);
            var argumentedUrl = SearchFiltersFactory.appendFieldFilters(url, menuData);
            // console.log(menuData);
            return RequestManager.getWithParams(url, { 'menu_id': menuData[1], 'meal': menuData[7] }, 'CC');

        }
    }
});
app.service('SearchManager', function (RequestManager, REQUEST, StorageManager, SignatureFactory) {
    return {
        getUser: function () {
            return StorageManager.getObject('user');
        },
        getMenu: function () {
            var url = SignatureFactory.appendSignature('forms/7', "GET", 3600);
            return RequestManager.get(url, 'GET');
        }
    }
});

app.service('MenuService', function () {
    var weekTitle;
    var Menu;
    var menuFilters;


    var setWeekTitle = function (newObj) {
        weekTitle = newObj;
    };

    var getWeekTitle = function () {
        return weekTitle;
    };

    var setMenu = function (newObj) {
        Menu = newObj;
    };

    var getMenu = function () {
        return Menu;
    };

    var setMenuFilters = function (newObj) {
        menuFilters = newObj;
    };

    var getMenuFilters = function () {
        return menuFilters;
    };

    return {
        setMenu: setMenu,
        getMenu: getMenu,
        setMenuFilters: setMenuFilters,
        getMenuFilters: getMenuFilters,
        setWeekTitle: setWeekTitle,
        getWeekTitle: getWeekTitle
    };

});
app.service('storageHelper', function () {
    var subject;
    var isLateplate = false;

    var getSubject = function () {
        return subject;
    };

    var setSubject = function (newObj) {
        subject = newObj;
    };


    var get_isLateplate = function () {
        return isLateplate;
    };
    var set_isLateplate = function (newObj) {
        isLateplate = newObj;
    };

    return {
        setSubject: setSubject,
        getSubject: getSubject,
        get_isLateplate: get_isLateplate,
        set_isLateplate: set_isLateplate
    };

});

app.service('UserInformationService', function () {
    var data;
    var cookie;
    var setUserId = function (newObj) {
        data = newObj;
    };

    var getUserId = function () {
        return data.entry_id;
    };

    var getData = function (key) {
        if ('string' !== typeof (key)) {
            return data;
        }
        return ('undefined' === typeof data[key]) ? null : data[key];
    };

    var setUserCookie = function (newObj) {
        cookie = newObj;
    };

    var getUserCookie = function () {
        return cookie;
    };


    return {
        setUserId: setUserId,
        getUserId: getUserId,
        getData: getData,
        setUserCookie: setUserCookie,
        getUserCookie: getUserCookie
    };

});

app.factory('getOrCreateUserInformationService', function ($q, UserManager, UserInformationService, DatabaseManager) {
    return {
        getOrCreateUserInformation: getOrCreateUserInformation,
        getStudentInformation: getStudentInformation,
        updateUserInformation: updateUserInformation
    };

    function getStudentInformation(studentId) {
        var deferred = $q.defer();
        var dbStudent = DatabaseManager.getStudentWithId(studentId);

        if (dbStudent.length != 0) {
            deferred.resolve(dbStudent[0]);
        }
        else {
            deferred.reject(null);
        }
        return deferred.promise;
    }

    function getOrCreateUserInformation() {
        var deferred = $q.defer();
        var studentId = UserInformationService.getUserId();
        var dbStudent = DatabaseManager.getStudentWithId(studentId);

        /* we will update this when wp user will have house and school info*/
        //$scope.studentData = DatabaseManager.getAllStudent()[0];

        if (dbStudent.length != 0 && 'undefined' !== typeof dbStudent[0].offCampus && '' !== dbStudent[0].offCampus) {
            deferred.resolve(dbStudent[0]);
        }
        else {
            /*inser user into local storage*/
            /* var createdStudent = {};
             createdStudent.id = studentId;
              UserManager.getStudentInformation(createdStudent).then(function (data) {
                 if (data.status == 200) {
                     var studentData = {};
                     var user = data.response.entries[0];
                     studentData.id = user.id;
                     studentData.name = user[1.3] + ' ' + user[1.6];
                     studentData.firstName = user[1.3];
                     studentData.lastName = user[1.6];
                     studentData.school = user[5];
                     studentData.house = user[6];
                     studentData.offCampus = 'No' === user[12];
                     DatabaseManager.inserStudent(studentData);
                     deferred.resolve(studentData);
                 }
                 else if (data.status == 400) {
                     deferred.reject(data.response)
                 } else {
                     deferred.reject('Unexpected server error.');
                 }
             }, function (err) {
                 deferred.reject(err);
             }); */
            var requestData = 'cookie=' + UserInformationService.getUserCookie();
            UserManager.getUserMeta(requestData).then(function (data) {
                if (data.status === 'ok') {
                    var studentData = {};
                    studentData.id = data.entry_id;
                    studentData.name = data.first_name + ' ' + data.last_name;
                    studentData.firstName = data.first_name;
                    studentData.lastName = data.last_name;
                    studentData.school = data.campus;
                    studentData.house = data.house;
                    studentData.offCampus = data.live_in_house === 'No';
                    DatabaseManager.inserStudent(studentData);
                    deferred.resolve(studentData);
                } else if (data.status == 400) {
                    deferred.reject(data.response)
                } else {
                    deferred.reject('Unexpected server error.');
                }
            }, function (err) {
                deferred.reject(err);
            })
        }
        return deferred.promise;
    }

    function updateUserInformation(updatedStudentInfo) {
        var studentData = {};
        studentData.id = UserInformationService.getUserId();
        studentData.firstName = updatedStudentInfo.firstName;
        studentData.lastName = updatedStudentInfo.lastName;
        studentData.name = updatedStudentInfo.firstName + ' ' + updatedStudentInfo.lastName;
        studentData.offCampus = updatedStudentInfo.input_12 === 'No';
        // studentData.liveInHouse = updatedStudentInfo.input_12;
        DatabaseManager.updateStudentInfo(studentData);
    }

});

app.factory('AnalyticsFactory', function ($ionicAnalytics) {

    return {
        'userError': userError
    };

    function userError(username, url, error) {
        return $ionicAnalytics.track('Deferred Reject', {
            userId: username,
            error_url: url,
            error_message: error
        });
    }
});
