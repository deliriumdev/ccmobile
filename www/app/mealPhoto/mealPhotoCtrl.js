(function () {
    'use strict';

    angular
        .module('starter')
        .config(addRoute)
        .controller('MealPhotoCtrl', mealPhotoCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.meal-photo', {
                url: '/meal/photo',
                views: {
                    'tab-meal-photo': {
                        templateUrl: 'app/mealPhoto/tab-meal-photo.html',
                        controller: 'MealPhotoCtrl as vm'
                    }
                }
            });
    }

    mealPhotoCtrl.$inject = ['$cordovaCamera','$cordovaSocialSharing' ,'$scope','$ionicPopup','$cordovaFileTransfer','$cordovaFile','ImageUploadService'];

    /* @ngInject */

    function mealPhotoCtrl($cordovaCamera,$cordovaSocialSharing,$scope,$ionicPopup,$cordovaFileTransfer,$cordovaFile,ImageUploadService) {
        /* jshint validthis: true */
        var vm = this;
        vm.title = 'Take Meal Photo';
        vm.takePicture = function () {
            var options = {
                quality : 60,
                destinationType : Camera.DestinationType.DATA_URL,
                sourceType : Camera.PictureSourceType.CAMERA,
                allowEdit : false,
                encodingType: Camera.EncodingType.JPEG,
                saveToPhotoAlbum: true
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                // Success! Image data is here
                vm.imgSrc = "data:image/jpeg;base64," + imageData;
                vm.imgData = imageData;
                if(vm.imgSrc){
                    vm.showConfirm();
                }

            }, function (err) {
                alert("An error occured: " + err);
            });
        };

        // A confirm dialog
        vm.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Send Photo to Campus Cooks',
                template: 'Are you sure you want to email this picture to Campus Cooks?'
            });
            confirmPopup.then(function(res) {
                if(res)
                {
                    console.log('You are sure');
                    $cordovaSocialSharing
                        .shareViaEmail(null,null, ['app@campuscooks.com'],null, null, vm.imgSrc)
                        .then(function(result) {
                            // Success!
                            console.log(result);
                        }, function(err) {
                            // An error occured. Show a message to the user
                            console.log(err);
                        });
                    //uploadToCloudinary(vm.imgData).then(function(data){
                    //    console.log(data)
                    //},
                    //function(err)
                    //{
                    //    console.log(err);
                    //})
                }
                else {
                    console.log('You are not sure');
                }
            });
        };

        var upload = function() {
            var options = {
                fileKey: "avatar",
                fileName: "image.png",
                chunkedMode: false,
                mimeType: "image/png"
            };
            $cordovaFileTransfer.upload("http://192.168.56.1:1337/file/upload", vm.imgSrc, options).then(function(result) {
                console.log("SUCCESS: " + JSON.stringify(result.response));
            }, function(err) {
                console.log("ERROR: " + JSON.stringify(err));
                $cordovaDialogs.alert('There was some error uploading file', 'Error Uploading')
                    .then(function() {
                        // callback success
                    });
            }, function (progress) {
                // constant progress updates
            });
        };

        // 6
        function onCopySuccess(entry) {
            console.log(entry);
        }

        function fail(error) {
            console.log(error);
        }

        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i=0; i < 5; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }

        function createFileEntry(fileURI) {
            window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
        }
        // 5
        function copyFile(fileEntry) {
            var name = fileEntry.substr(fileEntry.lastIndexOf('/') + 1);
            var newName = makeid() + name;

            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem2) {
                    fileEntry.copyTo(
                        fileSystem2,
                        newName,
                        onCopySuccess,
                        fail
                    );
                },
                fail);
        }

        function savefile(dataurl){
            var name = dataurl.substr(dataurl.lastIndexOf('/') + 1);
            var newName = makeid() + name +'.jpg';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
                function (fileSystem) {
                    fileSystem.root.getDirectory( cordova.file.dataDirectory, {create:true, exclusive: false},
                        function(directory) {
                            directory.root.getFile(newName, {create: true, exclusive: false},
                                function (fileEntry) {
                                    fileEntry.createWriter(function (writer) {
                                        console.log("Start creating image file");
                                        writer.seek(0);
                                        writer.write(dataurl);
                                        console.log("End creating image file. File created");
                                    }, fail);
                                }, fail);
                        }, fail);
                }, fail);
        }


        vm.selectPicture = function () {
            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                // Success! Image data is here
                vm.imgSrc = "data:image/jpeg;base64," + imageData;
            }, function (err) {
                alert("An error occured: " + err);
            });
        };

        var successUpload = function(result) {
            var url = result.secure_url || '';
            var urlSmall;
            if(result && result.eager[0]) urlSmall = result.eager[0].secure_url || '';
            $cordovaCamera.cleanup();
        };
        var errorFunction = function(reason){
            console.log(reason);
        };
        var uploadToCloudinary = function (Image){
            return ImageUploadService.uploadImage(Image).then(successUpload,errorFunction);
        }
    }
})();