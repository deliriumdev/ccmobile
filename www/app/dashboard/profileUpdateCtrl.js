(function () {
    'use strict';

    angular
        .module('dashboard')
        .config(addRoute)
        .controller('profileUpdateCtrl', profileUpdateCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider) {
        $stateProvider
            .state('tab.profile', {
                url: '/profile',
                views: {
                    'tab-dash': {
                        cache: false,
                        templateUrl: 'app/dashboard/templates/profile-update.html',
                        controller: 'profileUpdateCtrl',
                        resolve: {
                            studentData: function (getOrCreateUserInformationService) {
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    profileUpdateCtrl.$inject = ['$scope', 'studentData', 'UserManager', '$window', 'getOrCreateUserInformationService', '$http', 'UserInformationService', '$ionicScrollDelegate', 'alert'];

    /* @ngInject */
    function profileUpdateCtrl($scope, studentData, UserManager, $window, getOrCreateUserInformationService, $http, UserInformationService, $ionicScrollDelegate, alert) {
        console.log("profileUpdateCtrl....");
        $scope.options = ['Yes', 'No'];
        $scope.student = studentData;
        $scope.student.input_12 = studentData.offCampus ? $scope.options[1] : $scope.options[0];
        console.log('input_12:', studentData.offCampus);
        $scope.updateProfile = function (student) {
            var requestData = {
                'first_name': student.firstName,
                'last_name': student.lastName,
                'live_in_house': student.input_12,
                'cookie': UserInformationService.getUserCookie()
            };

            var successUpdating = function (data) {
                ///*inser user ID into object storage*/
                if (data.status == 'ok') {
                    getOrCreateUserInformationService.updateUserInformation(student);
                    alert.createAlert('info', null, 'Profile Updated Successfully!');
                }
                else {
                    $ionicScrollDelegate.scrollTop();
                    alert.createAlert('error', null, 'Error in request!');
                }
            };
            var errorUpdating = function (reason) {
                console.log(reason);
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('info', null, 'Error in request!');
            };
            UserManager.updateUserProfile(requestData).then(successUpdating, errorUpdating);
        };
    }

})();