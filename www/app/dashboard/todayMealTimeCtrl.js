(function () {
    'use strict';

    angular
        .module('dashboard')
        .config(addRoute)
        .controller('TodayMealTimeCtrl', getMealTimeCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.today-mealtime', {
                url: '/today/mealtime',
                views: {
                    'tab-dash': {
                        templateUrl: 'app/dashboard/templates/today-meal-time.html',
                        controller: 'TodayMealTimeCtrl'
                        //resolve:{
                        //    studentData:function(DatabaseManager,UserInformationService){
                        //        var studentID = UserInformationService.getUserId();
                        //        return DatabaseManager.getStudentWithId(studentID)[0];
                        //    }
                        //}
                    }
                }
            });
    }

    getMealTimeCtrl.$inject = ['$scope','UserManager','$state','MenuService','$ionicScrollDelegate','alert'];

    /* @ngInject */
    function getMealTimeCtrl($scope,UserManager, $state, MenuService,$ionicScrollDelegate,alert) {
        /* jshint validthis: true */
        //var vm = this;

        $scope.mealtime;
        var menu = MenuService.getMenuFilters();
        $scope.textMessage = 'Your Menu for Today';
        $scope.breakfast = false;
        $scope.lunch = false;
        $scope.dinner = false;
        $scope.loadMenuMessage = "Please wait while we retrieve your menu"
        var count = 0;


        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };

        var successBreakfast = function(data){
            if(data.response.entries[0]){
                $scope.breakfast = true;
                console.log( $scope.breakfast, $scope.lunch, $scope.dinner)
            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }

            }
        };
        var successLunch = function(data){
            if(data.response.entries[0]){
                $scope.lunch = true;
                console.log( $scope.breakfast, $scope.lunch, $scope.dinner)

            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }
            }
        };
        var successDinner = function(data){
            if(data.response.entries[0]){
                $scope.dinner = true;
                console.log( $scope.breakfast, $scope.lunch, $scope.dinner)

            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }
            }
        };
        menu[3] = 'Breakfast';
        UserManager.getStudentMenu(menu).then(successBreakfast,errorFunction);
        menu[3] = 'Lunch';
        UserManager.getStudentMenu(menu).then(successLunch,errorFunction);
        menu[3] = 'Dinner';
        UserManager.getStudentMenu(menu).then(successDinner,errorFunction);

        var successMenu = function(data){
            if(data.response.entries[0]){
                MenuService.setMenu(data.response.entries[0]);
                $state.go('tab.today-menu-detail');
            }
            else
            {
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('warning',null,'No menu for this time!');
            }
        };

        $scope.getMenu = function(mealtime){
            menu[3] = mealtime;
            UserManager.getStudentMenu(menu).then(successMenu,errorFunction);
        };

        $scope.showNoMenuAvailable_error = function(){

            if($scope.breakfast||$scope.lunch||$scope.dinner){
                return true
            }
        }
    }
})();