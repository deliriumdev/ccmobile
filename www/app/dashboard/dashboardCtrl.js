(function () {
    'use strict';

    angular
        .module('dashboard')
        .config(addRoute)
        .controller('DashboardCtrl', dashboardCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider) {
        $stateProvider
            .state('tab.dash', {
                cache: false,
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'app/dashboard/templates/dashboard.html',
                        controller: 'DashboardCtrl',
                        resolve: {
                            studentData: function (getOrCreateUserInformationService) {
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            },
                            getNotifications: function (UserManager) {
                                //var appVersion = $cordovaAppVersion.getVersionNumber()
                                var appVersion = "1.5.6"
                                return UserManager.getUserNotifications(appVersion);

                            }
                        }
                    }
                }
            });
    }

    dashboardCtrl.$inject = ['$scope', '$state', 'studentData', 'UserManager', 'MenuService', '$ionicScrollDelegate', 'alert',
        '$cordovaEmailComposer', 'DatabaseManager', '$ionicUser', '$cordovaInAppBrowser',
        'getNotifications', '$ionicModal'];

    /* @ngInject */
    function dashboardCtrl($scope, $state, studentData, UserManager, MenuService, $ionicScrollDelegate, alert,
        $cordovaEmailComposer, DatabaseManager, $ionicUser, $cordovaInAppBrowser,
        getNotifications, $ionicModal) {
        /* jshint validthis: true */
        //var vm = this;
        $scope.studentData = studentData;
        $scope.helpFormUser = {};
        $scope.email_helpForm = false;


        console.log("getNotifications : ", getNotifications)
        $scope.notifications = getNotifications;
        var userId;
        if (studentData.userId !== undefined) {
            userId = studentData.userId;
        }
        else {
            userId = studentData.id;
        }
        $ionicUser.identify({
            user_id: userId,
            name: studentData.name,
            house: studentData.house,
            school: studentData.school,
            message: 'I come from Campus cooks mobile Application'
        });

        //console.log(studentData);
        var errorFunction = function () {
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error', null, 'Internal Server Error!');
        };
        var successNextMeal = function (response) {
            if (response.success) {
                var menu = {};
                menu[2] = response.data[4];
                menu[35] = response.data[3];
                menu[7] = studentData.school;
                menu[8] = response.data[2];

                MenuService.setMenuFilters(menu);
                $state.go('tab.get-menu');
            }
            else {
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('info', null, 'No today meal!');
            }
        };
        $scope.getTodaysMeal = function () {
            var sId;
            if (studentData.userId) {
                sId = studentData.userId;
            }
            else {
                sId = studentData.id;
            }
            var requestData = 'entry_id=' + sId + '&action=get_todays_meal';
            UserManager.runWPAction(requestData).then(successNextMeal, errorFunction);
        };
        $scope.triggerEmail = function () {
            var email = {
                to: 'app@campuscooks.com',
                subject: 'Need Help!',
                body: '',
                isHtml: true
            };
            $cordovaEmailComposer.open(email).then(null,
                function () {
                    return;
                });
        };
        $scope.needHelp = function () {
            var options = {
                location: 'yes',
                clearcache: 'yes',
                toolbar: 'no'
            };
            $cordovaInAppBrowser.open('http://www.campuscooks.com', '_blank', options)
            //$cordovaInAppBrowser.close();
        }
        $scope.signOut = function () {
            var sId;
            if (studentData.userId) {
                sId = studentData.userId;
            }
            else {
                sId = studentData.id;
            }
            if (sId) {
                var removed_count = DatabaseManager.removeStudent(sId);
                if (removed_count > 0) {
                    $state.go('login');
                }
            }
        };
        if (window.cordova) {
            $cordovaEmailComposer.isAvailable().then(function () {
                //is available
                console.log("is available");
            }, function () {
                //not available
                console.log("not available");
            });
        }


        $scope.sendEmail = function () {
            var email = {
                to: 'app@campuscooks.com',
                subject: 'Need Help',
                body: '',
                isHtml: true
            };
            $cordovaEmailComposer.open(email)

        };

        //    modal

        $ionicModal.fromTemplateUrl('app/accounts/templates/help-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        $scope.sendHelpMessage = function (data) {
            data.user_id = userId;
            console.log(data)
        }


    }

})();