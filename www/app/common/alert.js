(function () {
    'use strict';

    angular
        .module('common')
        .factory('alert', alert);

    alert.$inject = ['$rootScope','$timeout'];

    /* @ngInject */
    function alert($rootScope, $timeout) {
        var alertTimeout;
        var service = {
            createAlert:createAlert
        };

    return service;

    ////////////////

    function createAlert(type,title,message,timeout) {
        $rootScope.alert ={
            hasBeenShown:true,
            show:true,
            type:type,
            message:message,
            title:title
        };
        $timeout.cancel(alertTimeout);
        alertTimeout = $timeout(function(){
            $rootScope.alert.show = false;
        },timeout ||2000);
    }
}
})();