(function () {
    'use strict';

    angular
        .module('accounts', ['ab-base64','ngMessages',

            'starter.services',
            'starter.utils',
            'customDirective',
            'common'
        ]);
})();