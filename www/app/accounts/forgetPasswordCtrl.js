(function () {
    'use strict';

    angular
        .module('accounts')
        .config(addRoute)
        .controller('ForgetPasswordCtrl', forgetPassword);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('forget-password', {
                url: "/forget-password",
                //abstract: true,
                templateUrl: "app/accounts/templates/forget-password.html",
                controller: 'ForgetPasswordCtrl  as vm'
            });
    }

    forgetPassword.$inject = ['$scope','UserManager','$ionicScrollDelegate','alert'];

    /* @ngInject */
    function forgetPassword($scope,UserManager,$ionicScrollDelegate,alert) {
        /* jshint validthis: true */
        //var vm = this;

        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };

        var successLogin = function(data){
            if(data.status == 'ok'){
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('info',null,'Please check your inbox for password reset link.!');
            }
            if(data.status == 'error'){
                $ionicScrollDelegate.scrollTop();
                console.log(data.message);
                alert.createAlert('warning',null,data.error);
            }
        };

        $scope.forgetPassword = function(user){
            var uri = "user_login="+user.email;
            var requestData = encodeURI(uri);
            UserManager.forgetPassword(requestData).then(successLogin,errorFunction);

        }
    }
})();