(function () {
    'use strict';

    angular
        .module('accounts')
        .config(addRoute)
        .controller('LoginCtrl', loginCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('login', {
                url: "/login",
                //abstract: true,
                templateUrl: "app/accounts/templates/login.html",
                controller: 'LoginCtrl  as vm',
                resolve: {
                    initDB: function(DatabaseManager){
                        return DatabaseManager.initDB();
                    },
                    user: function(DatabaseManager){
                        return DatabaseManager.getAllStudent()[0];
                    }
                }
            });
    }

    loginCtrl.$inject = ['$scope','UserManager','$location','UserInformationService','user','$ionicScrollDelegate',
        'alert','$state','$ionicModal'];

    /* @ngInject */
    function loginCtrl($scope,UserManager,$location,UserInformationService,user,$ionicScrollDelegate,alert,$state,
                       $ionicModal) {
        /* jshint validthis: true */
        //var vm = this;

        $scope.user = {};
        $scope.helpFormUser = {};
        $scope.email_helpForm = true;

        if(user){
            UserInformationService.setUserId(user.userId);
            //$state.go('tab.dash');
            //$location.path('tab/dash');
        }

        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };

        var successUserMeta = function(data){
            if(data.entry_id){
                UserInformationService.setUserId(data);
                $location.path('tab/dash');
            }
            else{
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('error',null,'No user exists. It is required to create a fresh account every year. Please create a new account');
            }
        };

        var successLogin = function(data){
            if(data.status == 'ok'){
                UserInformationService.setUserCookie(data.cookie);
                var requestData ='cookie='+data.cookie;
                UserManager.getUserMeta(requestData).then(successUserMeta,errorFunction);
            }
            else
            {
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('info',null,'No user exists. It is required to create a fresh account every year. Please create a new account');
            }
        };

        $scope.login = function(user){
            var uri = "username="+user.username+"&password="+user.password;
            var data = encodeURI(uri);
            UserManager.getAuthenticationCookie(data).then(successLogin,errorFunction);

        }

        //    modal

        $ionicModal.fromTemplateUrl('app/accounts/templates/help-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        $scope.sendHelpMessage = function (data) {
            console.log(data)
        }


    }
})();
