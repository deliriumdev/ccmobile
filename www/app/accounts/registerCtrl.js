(function () {
    'use strict';

    angular
        .module('accounts')
        .config(addRoute)
        .controller('RegisterCtrl', registerCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('register', {
                url: "/register",
                //abstract: true,
                templateUrl: "app/accounts/templates/register.html",
                controller: 'RegisterCtrl  as vm'
            });
    }

    registerCtrl.$inject = ['$scope'];

    /* @ngInject */
    function registerCtrl($scope) {
        /* jshint validthis: true */
        //var vm = this;

    }
})();