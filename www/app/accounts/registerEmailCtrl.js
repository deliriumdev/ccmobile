(function () {
    'use strict';

    angular
        .module('accounts')
        .config(addRoute)
        .controller('RegisterEmailCtrl', registerEmailCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            // route to show our basic form (/form)
            .state('email', {
                url: '/email',
                templateUrl: 'app/accounts/templates/form-email-registration.html',
                controller: 'RegisterEmailCtrl'
                ,
                resolve:{
                    groups:function(UserManager){
                        return UserManager.getGroups("isRequest=true&action=get_groups")
                    }
                }
            })

            // nested states
            // each of these sections will have their own view
            // url will be nested (/form/profile)

            .state('email.pin', {
                url: '/pin',
                templateUrl: 'app/accounts/templates/form_pin.html'
            })

            .state('email.profile', {
                url: '/profile',
                templateUrl: 'app/accounts/templates/form-profile.html'
            })

            // url will be /form/interests
            .state('email.interests', {
                url: '/interests',
                templateUrl: 'app/accounts/templates/form-interests.html'
            })
            .state('email.allergies', {
                url: '/allergies',
                templateUrl: 'app/accounts/templates/form-allergies.html'
            })
            // url will be /form/payment
            .state('email.additional', {
                url: '/additional',
                templateUrl: 'app/accounts/templates/form-additional.html'
            })
            .state('email.confirmation', {
                url: '/confirmation',
                templateUrl: 'app/accounts/templates/form-email-confirmation.html'
            });
    }

    registerEmailCtrl.$inject = ['$scope','groups','helperFunctionsFactory','UserManager','$location','FormManager',
        '$ionicScrollDelegate','alert','$state','$ionicModal'];

    /* @ngInject */
    function registerEmailCtrl($scope,groups,helperFunctionsFactory,UserManager,$location,FormManager,
                               $ionicScrollDelegate,alert,$state,$ionicModal) {
        /* jshint validthis: true */
        //var vm = this;
        $scope.helpFormUser = {};
        $scope.email_helpForm = true;
        $scope.options = ['Yes','No'];
        $scope.group = groups;
        $scope.school = groups.filter(helperFunctionsFactory.schoolFilter);
        FormManager.getRegistrationForm().then(function(data){
                //console.log(data.response.fields);
                $scope.form = data.response;
                var fields = data.response.fields;

                $scope.form = {
                    'firstName'         : 'input_1_3',
                    'lastName'          : 'input_1_6',
                    'phone'             : 'input_' + fields[1].id,
                    'email'             : 'input_' + fields[2].id,
                    'password'          : 'input_' + fields[3].id,
                    'confirmPassword'   : 'input_' + fields[3].id + '_2',
                    'campus'            : 'input_' + fields[4].id,
                    'house'             : 'input_' + fields[5].id,
                    'favoriteDishes'    : 'input_' + fields[7].id,
                    'foodAllergies'     : 'input_' + fields[8].id,
                    'additionalInfo'    : 'input_' + fields[9].id,
                    'doctorDiagnosed'   : 'input_' + fields[11].id,
                    'allergy'           : 'input_' + fields[12].id,
                    'specificItem'      : 'input_' + fields[13].id,
                    'severityOfAllergy' : 'input_' + fields[14].id,
                    'carry'             : 'input_' + fields[15].id,
                    'isAllergy'         : false
                };
                //console.log($scope.form);


                //$scope.button = data.response.button;

            },function (err){
                alert(err);
            }
        );


        var errorRegistration = function(reason){
            console.log(reason);
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('info',null,'Error in request!');
        };

        $scope.student = {};
        $scope.register = function(student){
            console.log('in email registration.');

//            var school = student.input_5;
//            student.input_5 = school.name;
//            var house = student.input_6;
//            student.input_6 = house.name;
            var requestData = {'input_values': student};

            var successRegistration = function(data){
                ///*inser user ID into object storage*/
                if (data.status == 200){
                    if(!data.response.is_valid){
                        var serverResponse = data.response.validation_messages;
                        var errorMessage = null;
                        for (var fieldName in serverResponse) {
                            var message = serverResponse[fieldName];
                            errorMessage = errorMessage + message;
                        }
                        $ionicScrollDelegate.scrollTop();
                        alert.createAlert('error',null,errorMessage);
                    }
                    else if (data.response.is_valid){
                        console.log(data.response.confirmation_message);
                        if (data.response.confirmation_message){
                            $location.path('email/confirmation');
                        }
                    }
                    else {
                        $ionicScrollDelegate.scrollTop();
                        alert.createAlert('error',null,'Error in request!');
                    }
                }
                else {
                    $ionicScrollDelegate.scrollTop();
                    alert.createAlert('error',null,'Error in request!');
                }
            };

            UserManager.registerUser(requestData).then(successRegistration,errorRegistration);
        };

        var formEncode = function(data) {
            var pairs = [];
            for (var name in data) {
                pairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
            }
            return pairs.join('&').replace(/%20/g, '+');
        };

        $scope.CheckPIN = function(student){
            var successPIN = function(data){
                if(data.success){
                    $scope.student[$scope.form.campus] = data.data.campus;
                    $scope.student[$scope.form.house] = data.data.house;
                    $state.go('email.profile');
                    $ionicScrollDelegate.resize()
                }
                else{
                    $ionicScrollDelegate.scrollTop();
                    alert.createAlert('warning',null,'Invalid Pin!');
                }
            };


//            var school = student.input_5;
//            var house = student.input_6;
            var requestData = {'user_pin':student.pin};
            UserManager.pinVerification(formEncode(requestData)).then(successPIN,errorRegistration);
        };

        //    modal

        $ionicModal.fromTemplateUrl('app/accounts/templates/help-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        $scope.sendHelpMessage = function (data) {
            console.log(data)
        }



    }
})();