(function () {
    'use strict';

    angular
        .module('accounts')
        .config(addRoute)
        .controller('RegisterFacebookCtrl', registerFacebookCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('facebook', {
                url: "/register/facebook",
                templateUrl: "app/accounts/templates/facebookRegister.html",
                controller: 'RegisterFacebookCtrl  as vm'
            });
    }

    registerFacebookCtrl.$inject = [];

    /* @ngInject */
    function registerFacebookCtrl() {
        /* jshint validthis: true */
        var vm = this;

    }
})();