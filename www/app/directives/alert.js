(function () {
    'use strict';

    angular
        .module('customDirective')
        .directive('alert', alert);

    alert.$inject = ['$window'];

    /* @ngInject */
    function alert($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            //link: link,
            restrict: 'E',
            templateUrl:'app/directives/templates/alert-template.html'
        };
        return directive;

        //function link(scope, element, attrs) {
        //}
    }
})();