(function () {
    'use strict';

    angular
        .module('menu')
        .config(addRoute)
        .controller('GetMenuCtrl', getMenuCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.get-menu', {
                url: '/menu',
                views: {
                    'tab-get-menu': {
                        templateUrl: 'app/menu/templates/tab-get-menu.html',
                        controller: 'GetMenuCtrl',
                        resolve:{
                            weekOptions:function(FormManager){
                                return FormManager.getMenuWeekOptions();
                            },
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    getMenuCtrl.$inject = ['$scope','$rootScope','$state','UserManager','$location','FormManager','MenuService','weekOptions','$filter','studentData','helperFunctionsFactory','$ionicScrollDelegate','alert','storageHelper'];

    /* @ngInject */
    function getMenuCtrl($scope,$rootScope,$state,UserManager, $location, FormManager, MenuService, weekOptions,$filter,studentData,helperFunctionsFactory,$ionicScrollDelegate,alert,storageHelper) {
        /* jshint validthis: true */
        //var vm = this;
        $scope.dayOption = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

        var arr =[];
        var today_menu = {};
        var  currentDate = new Date();
        var count = 0;
        var nextWeekDaysList = [];
        var currentWeekDaysList = [];
        //console.log($filter('date')(currentDate,'EEEE'))
        //console.log($scope.dayOption.indexOf($filter('date')(currentDate,'EEEE')))
        $scope.viewCondition = $scope.dayOption.indexOf($filter('date')(currentDate,'EEEE'));
        $scope.loadMenuMessage = "Please wait while we retrieve your menu";
        $scope.dateList = [];
        $scope.prevButton = false;
        $scope.nextButton = true;
        $scope.showRateMealBtn = true;



        function getDatebyWeek(days,weekDate){
            var date = new Date(weekDate);
            var newDate = date.setDate(date.getDate()+days)
            return newDate
        }

        function getDate(days){
            var date = new Date(weekOptions[0]);
            var newDate = date.setDate(date.getDate()+days)
            return newDate
        }
        //for current weeek days
        $scope.dayOption.forEach(function(item,index){
            var obj = {};
            var date = getDate(index)
            obj.id = index,
                obj.date = $filter('date')(date,'MMM d');
            obj.day =$filter('date')(date,'EEE');
            obj.week = weekOptions[0];
            currentWeekDaysList.push(obj);
        });
        $scope.dateList = currentWeekDaysList;
        //for next weeek days
        $scope.dayOption.forEach(function(item,index){
            var obj = {};
            var date = getDatebyWeek(index,weekOptions[1])
            obj.id = index,
                obj.date = $filter('date')(date,'MMM d');
            obj.day =$filter('date')(date,'EEE');
            obj.week = weekOptions[1];
            nextWeekDaysList.push(obj);
        });

        $scope.nextweek = function(){
            $scope.breakfast = false;
            $scope.lunch = false;
            $scope.dinner = false;
            $scope.loadMenuMessage = "";
            $scope.prevButton = true;
            $scope.nextButton = false;
            $scope.viewCondition = 10;
            $scope.dateList = nextWeekDaysList;
        }
        $scope.previousweek= function(){
            $scope.breakfast = false;
            $scope.lunch = false;
            $scope.dinner = false;
            $scope.loadMenuMessage = "";
            $scope.prevButton = false;
            $scope.nextButton = true;
            $scope.viewCondition = 10;
            $scope.dateList = currentWeekDaysList;
        }

        if(weekOptions){

            var template1 = {};
            template1.text =  'This Week';
            template1.value = weekOptions[0];
            arr.push(template1);
            var template = {};
            template.text = 'Next Week';
            template.value = weekOptions[1];
            arr.push(template);
        }

        $scope.weekOptions = arr;
        //var successMenuForm = function(data){
        //    var form = [];
        //    $scope.form = data.response;
        //    var result = data.response.fields;
        //
        //    for(var i=0; i<result.length;i++){
        //        if(result[i].id == 2){
        //            result[i].label = 'Select Day';
        //            form.push(result[i]);
        //        }
        //        if(result[i].id == 34){
        //            result[i].label = 'Select Week';
        //            result[i].choices = arr;
        //            form.push(result[i]);
        //        }
        //    }
        //
        //    $scope.form.fields =form;
        //
        //};
        //var errorFunction = function(err){
        //    alert(err);
        //};
        //FormManager.getMenuForm().then(successMenuForm,errorFunction);

        $scope.menu = {};
        //menu = Object {2: "Wednesday", 34: "06/20/2016"
        $scope.getMealtime = function(menu){
            if(menu[34]==arr[0].value){
                MenuService.setWeekTitle('This Week');
            }
            else{
                MenuService.setWeekTitle('Next Week');
            }
            MenuService.setMenuFilters(menu);
            $location.path('tab/menu/mealtime');
        }

        //

        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };
        var successBreakfast = function(data){
            if(data.response.entries[0]){
                $scope.breakfast = true;
                $scope.meal_breakfast = getMealtimeDetail(data.response.entries[0])
            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }
            }
        };
        var successLunch = function(data){
            if(data.response.entries[0]){
                $scope.lunch = true;
                $scope.meal_lunch = getMealtimeDetail(data.response.entries[0])
            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }
            }
        };
        var successDinner = function(data){
            if(data.response.entries[0]){
                $scope.dinner = true;
                $scope.meal_dinner = getMealtimeDetail(data.response.entries[0])
            }else{
                count++;
                if(count == 3){
                    $scope.loadMenuMessage = "No Menu Available"
                }
            }
        };
        today_menu[34] = weekOptions[0];
        today_menu[2] = $scope.dayOption[$scope.viewCondition];
        today_menu[8] = studentData.house; // student house
        today_menu[3] = 'Breakfast';
        UserManager.getStudentMenu(today_menu).then(successBreakfast,errorFunction);
        // check if out of house for lunch and dinner
        // console.log(studentData);
        if (studentData.offCampus) {
            UserManager.requestHouseData(studentData.house).then(function(houseData) {
            if ('1' === houseData.ocRestrict) {
                today_menu[45] = 'Yes';
            }
            today_menu[3] = 'Lunch';
            UserManager.getStudentMenu(today_menu).then(successLunch,errorFunction);
            today_menu[3] = 'Dinner';
            UserManager.getStudentMenu(today_menu).then(successDinner,errorFunction);
            }, errorFunction);
        }
        else { // fetch without querying out of house param
            // console.log('Getting with ', today_menu);
            today_menu[3] = 'Lunch';
            UserManager.getStudentMenu(today_menu).then(successLunch,errorFunction);
            today_menu[3] = 'Dinner';
            UserManager.getStudentMenu(today_menu).then(successDinner,errorFunction);
        }

        $scope.getmenu = function(item){
            var menu = {};
            $scope.breakfast = false;
            $scope.lunch = false;
            $scope.dinner = false;
            $scope.showRateMealBtn = false;
            //var selectedDate =  $filter('date')(getDatebyWeek(item.id,weekOptions[0]),'shortDate');
            var selectedDate =  $filter('date')(getDatebyWeek(item.id,item.week),'shortDate');
            if(selectedDate <= $filter('date')(currentDate,'shortDate')){
                console.log("*****date is small*****")
                $scope.showRateMealBtn = true;
            }
            //console.log("currentDate",$filter('date')(currentDate,'shortDate'),"selectedDate",$filter('date')(getDatebyWeek(item.id,weekOptions[0]),'shortDate'))
            console.log("currentDate",$filter('date')(currentDate,'shortDate'),"selectedDate",$filter('date')(getDatebyWeek(item.id,item.week),'shortDate'))
            count = 0;
            $scope.loadMenuMessage = "Please wait while we retrieve your menu";


            $scope.viewCondition = item.id;
            console.log(item)
            menu[34] = item.week;
            menu[2] = $scope.dayOption[item.id]
            menu[8] = studentData.house;
            menu[3] = 'Breakfast';
            UserManager.getStudentMenu(menu).then(successBreakfast,errorFunction);
            // check if out of house for lunch and dinner
            if (studentData.offCampus) {// pull the house setting
                UserManager.requestHouseData(studentData.house).then(function(houseData) {
                    if ('1' === houseData.ocRestrict) {
                        menu[45] = 'Yes';
                    }
                    menu[3] = 'Lunch';
                    UserManager.getStudentMenu(menu).then(successLunch,errorFunction);
                    menu[3] = 'Dinner';
                    UserManager.getStudentMenu(menu).then(successDinner,errorFunction);
                }, errorFunction);
            }
            else { // fetch without querying out of house param
                menu[3] = 'Lunch';
                UserManager.getStudentMenu(menu).then(successLunch,errorFunction);
                menu[3] = 'Dinner';
                UserManager.getStudentMenu(menu).then(successDinner,errorFunction);
            }



        }
        function getMealtimeDetail(data){


            var meal = {};
            var menuItem = {};
            menuItem[1] = data.id; //menu id
            menuItem[4] = studentData.name; //student name
            menuItem[5] = studentData.school; //student school
            menuItem[6] = studentData.house; // student house
            if (data) {
                meal.typeArray = [false,false,false];// gluten free,vegetarian,dairy free
                meal.foodArray = [];
                meal.id = data.id;
                meal.day = data[2];
                meal.mealDate = MenuService.getWeekTitle();
                meal.mealtime = data[3];
                meal.chefEmail = data[6];

                meal.school = data[7];
                meal.house = data[8];
                meal.chef = data[5];
                meal.complete_data = data;

                var prepareFoodItem = function(itemId,typeId,data,menuItem){
                    var foodObject = {};
                    foodObject['id'] = itemId;
                    foodObject['item'] = data[itemId];
                    foodObject['food_type'] = helperFunctionsFactory.CheckForType(typeId, data);// 14...
                    return foodObject;
                };
                if(data[10]){
                    var Entree = prepareFoodItem(10,14,data,menuItem);
                    meal.foodArray.push(Entree);
                }
                if(data[16]){
                    var sideDish = prepareFoodItem(16,18,data,menuItem);
                    meal.foodArray.push(sideDish);
                }
                if(data[19]){
                    var soup = prepareFoodItem(19,21,data,menuItem);
                    meal.foodArray.push(soup);
                }
                if(data[25]){
                    var vegetable = prepareFoodItem(25,27,data,menuItem);
                    meal.foodArray.push(vegetable);
                }
                if(data[28]){
                    var starch = prepareFoodItem(28,30,data,menuItem);
                    meal.foodArray.push(starch);
                }
                if(data[22]){
                    var additionalItem = prepareFoodItem(22,24,data,menuItem);
                    meal.foodArray.push(additionalItem);
                }
                if(data[31]){
                    var dessertItem = prepareFoodItem(31,33,data,menuItem);
                    meal.foodArray.push(dessertItem);
                }
            }
            return meal
        }
        var successMealAttending = function(data){
            $ionicScrollDelegate.scrollTop();
            //console.log(data);
            if (!data.response.is_valid && 'undefined' !== typeof(data.response.validation_messages)) {
                var errorMessages = [];
                for (var fieldName in data.response.validation_messages) {
                    errorMessages.push(data.response.validation_messages[fieldName]);
                }
                alert.createAlert('info', null, (0 == errorMessages.length) ? 'An unexpected error has occurred' : errorMessages.join(', '));
                return;
            }
            alert.createAlert('success',null,"Thank you. We'll let the chef know!");
        };
        $scope.mealAttending = function(meal){
            var data = meal.complete_data;
            var attendingData = {};
            attendingData['input_1'] = data.id; //Meal ID: 1
            attendingData['input_2'] = data[34]; //Week: 2
            attendingData['input_3'] = data[2]; //Day: 3
            attendingData['input_4'] = data[3]; //Meal Time: 4 (Breakfast, etc.)
            attendingData['input_6'] = studentData.name; //Student Name: 6
            attendingData['input_5'] = data[6]; //Chef Email: 5
            attendingData['input_7'] = data[7]; //student campus
            attendingData['input_8'] = data[8]; //student house
            attendingData['input_12'] = data[35]; //request date
            if ('' === attendingData['input_12']) {
                attendingData['input_12'] = data[34]; // monday is blank for some reason
            }
            attendingData['student_id'] = studentData.userId;
            var requestData = {'input_values':attendingData};
            UserManager.requestAttending(requestData).then(successMealAttending,errorFunction);
        };
        $scope.rateMenuItem = function(meal){
            var data = meal.complete_data;
            if(data[34]==weekOptions[0]){
                MenuService.setWeekTitle('This Week');
            }
            else{
                MenuService.setWeekTitle('Next Week');
            }
            MenuService.setMenu(data);
            $rootScope.redirectFlag = true;
            $state.go('tab.rate-items');
        };
        $scope.submitLatePlate = function(meal){
            var requestData = {house :studentData.house};
            UserManager.is_lateplateEnable(requestData).then(
                function(response){
                    if(response.success){
                        var data = meal.complete_data;
                        MenuService.setMenu(data);
                        storageHelper.set_isLateplate(true);
                        $state.go('tab.late-plate');
                    }else{
                        $ionicScrollDelegate.scrollTop();
                        alert.createAlert('warning',null,"If you are looking to set up late plate accommodations with Campus Cooks we would be glad to help. Please contact your house's coordinator to get things in place");
                    }
                });
        };
    }
})();
