(function () {
    'use strict';

    angular
        .module('menu')
        .config(addRoute)
        .controller('MenuDetailCtrl', menuDetailCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.menu-detail', {
                cache: false,
                url: '/menu/detail',
                views: {
                    'tab-get-menu': {
                        templateUrl: 'app/menu/templates/menu-details.html',
                        controller: 'MenuDetailCtrl',
                        resolve :{
                            studentData: function(getOrCreateUserInformationService){
                                return getOrCreateUserInformationService.getOrCreateUserInformation();

                            }
                        }
                    }
                }
            });
    }

    menuDetailCtrl.$inject = ['$scope','UserManager','FormManager',
        'MenuService','helperFunctionsFactory','studentData','$ionicScrollDelegate','alert','storageHelper','$state','$rootScope'];

    /* @ngInject */
    function menuDetailCtrl($scope,UserManager, FormManager, MenuService, helperFunctionsFactory,studentData,$ionicScrollDelegate,alert,storageHelper,$state,$rootScope) {
        /* jshint validthis: true */
        //var vm = this;

        var data = MenuService.getMenu();
        var menuItem = {};
        menuItem[1] = data.id; //menu id
        menuItem[4] = studentData.name; //student name
        menuItem[5] = studentData.school; //student school
        menuItem[6] = studentData.house; // student house

        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };
        var successNextMeal = function(response){
            if(response.success)
            {
                data = response.data;
            }
            else
            {
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('info',null,'No next meal!');
            }
        };
        $scope.getNextmeal = function() {
            var requestData = 'mealid='+data.id+'&action=get_next_meal';
            UserManager.runWPAction(requestData).then(successNextMeal,errorFunction);
        };

        var successMealAttending = function(data){
            $ionicScrollDelegate.scrollTop();
            //console.log(data);
            if (!data.response.is_valid && 'undefined' !== typeof(data.response.validation_messages)) {
                var errorMessages = [];
                for (var fieldName in data.response.validation_messages) {
                    errorMessages.push(data.response.validation_messages[fieldName]);
                }
                alert.createAlert('info', null, (0 == errorMessages.length) ? 'An unexpected error has occurred' : errorMessages.join(', '));
                return;
            }
            alert.createAlert('success',null,"Thank you. We'll let the chef know!");
        };
        $scope.mealAttending = function(){
            var attendingData = {};
            attendingData['input_1'] = data.id; //Meal ID: 1
            attendingData['input_2'] = data[34]; //Week: 2
            attendingData['input_3'] = data[2]; //Day: 3
            attendingData['input_4'] = data[3]; //Meal Time: 4 (Breakfast, etc.)
            attendingData['input_6'] = studentData.name; //Student Name: 6
            attendingData['input_5'] = data[6]; //Chef Email: 5
            attendingData['input_7'] = data[7]; //student campus
            attendingData['input_8'] = data[8]; //student house
            attendingData['input_12'] = data[35]; //request date
            if ('' === attendingData['input_12']) {
                attendingData['input_12'] = data[34]; // monday is blank for some reason
            }
            attendingData['student_id'] = studentData.userId;
            var requestData = {'input_values':attendingData};
            UserManager.requestAttending(requestData).then(successMealAttending,errorFunction);
        };

        if (data) {
            $scope.typeArray = [false,false,false];// gluten free,vegetarian,dairy free
            $scope.foodArray = [];
            $scope.id = data.id;
            $scope.day = data[2];
            $scope.mealDate = MenuService.getWeekTitle();
            $scope.mealtime = data[3];
            $scope.chefEmail = data[6];

            $scope.school = data[7];
            $scope.house = data[8];
            $scope.chef = data[5];

            var prepareFoodItem = function(itemId,typeId,data,menuItem){
                var foodObject = {};
                foodObject['id'] = itemId;
                foodObject['item'] = data[itemId];
                foodObject['food_type'] = helperFunctionsFactory.CheckForType(typeId, data);// 14...
                return foodObject;
            };


            if(data[10]){
                var Entree = prepareFoodItem(10,14,data,menuItem);
                $scope.foodArray.push(Entree);
            }
            if(data[16]){
                var sideDish = prepareFoodItem(16,18,data,menuItem);
                $scope.foodArray.push(sideDish);
            }
            if(data[19]){
                var soup = prepareFoodItem(19,21,data,menuItem);
                $scope.foodArray.push(soup);
            }
            if(data[25]){
                var vegetable = prepareFoodItem(25,27,data,menuItem);
                $scope.foodArray.push(vegetable);
            }
            if(data[28]){
                var starch = prepareFoodItem(28,30,data,menuItem);
                $scope.foodArray.push(starch);
            }
            if(data[22]){
                var additionalItem = prepareFoodItem(22,24,data,menuItem);
                $scope.foodArray.push(additionalItem);
            }
            if(data[31]){
                var dessertItem = prepareFoodItem(31,33,data,menuItem);
                $scope.foodArray.push(dessertItem);
            }
        }

        $scope.rateMenuItem = function(){
            $rootScope.redirectFlag = true;
            $state.go('tab.rate-items');
        };

        $scope.submitLatePlate = function(){
            var requestData = {house :studentData.house};
            UserManager.is_lateplateEnable(requestData).then(
                function(response){
                   if(response.success){
                       storageHelper.set_isLateplate(true);
                       $state.go('tab.late-plate');
                   }else{
                       $ionicScrollDelegate.scrollTop();
                       alert.createAlert('warning',null,"If you are looking to set up late plate accommodations with Campus Cooks we would be glad to help. Please contact your house's coordinator to get things in place");
                   }
                });
        };
    }
})();
