(function () {
    'use strict';

    angular
        .module('menu')
        .config(addRoute)
        .controller('GetMealTimeCtrl', getMealTimeCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.get-mealtime', {
                url: '/menu/mealtime',
                views: {
                    'tab-get-menu': {
                        templateUrl: 'app/menu/templates/tab-get-mealtime.html',
                        controller: 'GetMealTimeCtrl',
                        resolve:{
                            studentData:function(DatabaseManager,UserInformationService){
                                var studentID = UserInformationService.getUserId();
                                return DatabaseManager.getStudentWithId(studentID)[0];
                            }
                        }
                    }
                }
            });
    }

    getMealTimeCtrl.$inject = ['$scope','UserManager','$location','MenuService','studentData','$ionicScrollDelegate','alert'];

    /* @ngInject */
    function getMealTimeCtrl($scope,UserManager, $location, MenuService, studentData,$ionicScrollDelegate,alert) {
        /* jshint validthis: true */
        //var vm = this;

        $scope.mealtime;
        var menu = MenuService.getMenuFilters();
        menu[7] = studentData.school;
        menu[8] = studentData.house;
        $scope.textMessage = 'Your Menu for ' + menu[2] + ' ' + MenuService.getWeekTitle();
        $scope.breakfast = false;
        $scope.lunch = false;
        $scope.dinner = false;
        $scope.loadMenuMessage = "Please wait while we retrieve your menu"


        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };

        var successBreakfast = function(data){
            if(data.response.entries[0]){
                $scope.breakfast = true;
            }else{
                $scope.loadMenuMessage = "No Menu Available"
            }
        };
        var successLunch = function(data){
            if(data.response.entries[0]){
                $scope.lunch = true;
            }else{
                $scope.loadMenuMessage = "No Menu Available"
            }
        };
        var successDinner = function(data){
            if(data.response.entries[0]){
                $scope.dinner = true;
            }else{
                $scope.loadMenuMessage = "No Menu Available"
            }
        };
        menu[3] = 'Breakfast';
        UserManager.getStudentMenu(menu).then(successBreakfast,errorFunction);
        menu[3] = 'Lunch';
        UserManager.getStudentMenu(menu).then(successLunch,errorFunction);
        menu[3] = 'Dinner';
        UserManager.getStudentMenu(menu).then(successDinner,errorFunction);

        var successMenu = function(data){
            if(data.response.entries[0]){
                MenuService.setMenu(data.response.entries[0]);
                $location.path('tab/menu/detail');
            }
            else
            {
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('warning',null,'No menu for this time!');
            }
        };

        $scope.getMenu = function(mealtime){
            menu[3] = mealtime;
            UserManager.getStudentMenu(menu).then(successMenu,errorFunction);
        };
    }
})();
