(function () {
    'use strict';

    angular
        .module('menu', [
            'customDirective',
            'starter.services',
            'starter.utils',
            'common'
        ]);
})();