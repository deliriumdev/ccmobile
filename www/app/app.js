// Ionic Starter App
angular
    .module('starter', ['ngCordova',
        'ionic',
        'starter.services',
        'dashboard',
        'starter.config',
        'starter.utils',
        'accounts',
        'rateMeal',
        'menu',
        'ngMessages',
        'latePlate',
        'community',
        'ionic.service.core',
        'ionic.service.analytics'
    ])

    .run(function($ionicPlatform,DatabaseManager,$rootScope,$state,$location,$ionicAnalytics) {
        $ionicPlatform.ready(function() {

            //$ionicAnalytics.register();

            // Update app code with new release from Ionic Deploy
            //var deploy = new Ionic.Deploy();
            //console.log('Ionic Deploy: Checking for updates');
            //deploy.check().then(function(hasUpdate) {
            //    console.log('Ionic Deploy: Update available: ' + hasUpdate);
            //    deploy.update().then(function(res) {
            //        console.log('Ionic Deploy: Update Success! ', res);
            //    }, function(err) {
            //        console.log('Ionic Deploy: Update error! ', err);
            //    }, function(prog) {
            //        console.log('Ionic Deploy: Progress... ', prog);
            //    });
            //}, function(err) {
            //    console.error('Ionic Deploy: Unable to check for updates', err);
            //});

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            cordova.getAppVersion(function(version) {
                $rootScope.appVersion = version;
                console.log("appVersion in ready function : ",$rootScope.appVersion)
            });

            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            $rootScope.redirectFlag = false;


            var user = DatabaseManager.getAllStudent()[0];
            if(user)
            {
                var isDataWiped = localStorage.getItem('isDataWiped');
                if(isDataWiped) {
                    $state.go("tab.dash");
                }
                else
                {
                    localStorage.setItem('isDataWiped',true);
                    var removed_count = DatabaseManager.removeStudent(user.userId);
                    if(removed_count > 0){
                        $state.go('login');
                    }
                }
            }


            $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams)
            {
                if (toState.name =='tab.rate-items' && !(fromState.name =='tab.rate-items')){
                    //if (fromState.name == 'tab.menu-detail' && $rootScope.redirectFlag){
                    if (fromState.name == 'tab.get-menu' && $rootScope.redirectFlag){
                        $rootScope.redirectFlag = false;
                    }
                    else
                    {
                        if(!(fromState.name == 'tab.rate-mealtime')){
                            event.preventDefault();
                            $state.go("tab.rate-week");
                        }
                    }
                }
                else if (toState.name == 'login'){
                    var user = DatabaseManager.getAllStudent()[0];
                    if(user)
                    {
                        event.preventDefault();
                        $state.go("tab.dash");
                    }
                }
            });

        });

    })

    .config(function($stateProvider, $urlRouterProvider,configProvider,ENVIRONMENT,$ionicConfigProvider) {

        configProvider.setEnvironment(ENVIRONMENT.staging);
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.navBar.alignTitle('center');
        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html",
                controller:'TabCtrl'
            });
        $urlRouterProvider.otherwise('/login');

    })

    .controller('TabCtrl', function($scope,$state) {
        $scope.onLatePlate = function(){
            console.log("onLatePlate")
            $state.go('tab.late-plate-main')
        }
    });