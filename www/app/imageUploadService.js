(function() {

  /**
   * @ngInject
   */

  ius.$inject = ['$q', '$ionicLoading', '$cordovaFile', 'CLOUDINARY_CONFIGS','RequestManager'];

  function ius($q, $ionicLoading, $cordovaFile, CLOUDINARY_CONFIGS,RequestManager)
  {

    var service = {};

    service.uploadImage = uploadImage;

    return service;

    function uploadImage(imageURI)
    {
        var requestData = {'base64File':imageURI};
        return RequestManager.wp_post('wp-admin/admin-ajax.php?action=upload_image',requestData)

    }

  }

  angular.module('starter').factory('ImageUploadService', ius);

})();