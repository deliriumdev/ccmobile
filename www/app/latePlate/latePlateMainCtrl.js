(function () {
    'use strict';

    angular
        .module('latePlate')
        .config(addRoute)
        .controller('latePlateMainCtrl', latePlateMainCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.late-plate-main', {
                url: '/main/latePlate',
                views: {
                    'tab-late-plate': {
                        cache: false,
                        templateUrl: 'app/latePlate/templates/latePlate-type-selection.html',
                        controller: 'latePlateMainCtrl as vm',
                        resolve :{
                            is_lateplateEnable:function(UserManager,getOrCreateUserInformationService,UserInformationService){
                                return getOrCreateUserInformationService.getStudentInformation(UserInformationService.getUserId()).then(function(data){
                                    var requestData = {house :data.house};
                                    return UserManager.is_lateplateEnable(requestData).then(
                                        function(data){
                                            console.log('is enable');
                                            console.log(data);
                                            return data;
                                        });
                                });
                            }
                        }
                    }
                }
            });
    }


    latePlateMainCtrl.$inject = ['is_lateplateEnable'];

    /* @ngInject */
    function latePlateMainCtrl(is_lateplateEnable) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'Late Plate Selection';
        vm.is_lateplateEnable = is_lateplateEnable.success;

        activate();

        ////////////////

        function activate() {
            console.log('in main late plate selection');
        }
    }
})();