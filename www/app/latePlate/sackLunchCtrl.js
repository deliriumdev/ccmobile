(function () {
    'use strict';

    angular
        .module('latePlate')
        .config(addRoute)
        .controller('sackLunchCtrl', sackLunchCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.sack-lunch', {
                url: '/sack/lunch',
                views: {
                    'tab-late-plate': {
                        templateUrl: 'app/latePlate/templates/sackLunch-form-template.html',
                        controller: 'sackLunchCtrl as vm',
                        resolve :{
                            studentID:function(UserInformationService){
                                return UserInformationService.getUserId();
                            }
                        }
                    }
                }
            });
    }

    sackLunchCtrl.$inject = ['UserManager','studentID','getOrCreateUserInformationService','$timeout','$location'];

    /* @ngInject */
    function sackLunchCtrl(UserManager,studentID,getOrCreateUserInformationService,$timeout,$location) {
        /* jshint validthis: true */
        var vm = this;
        var items = ['date', 'time', 'confirmation'];
        vm.title = 'Sack Lunch';
        vm.sackLunch ={};

        vm.form = {
            terminationDate:11,
            reoccurring:10,
            date:3,
            time:7,
            message:6
        };

        vm.sackLunch[vm.form.reoccurring]= false;
        vm.weekOption = ['This Week','Next Week'];
        vm.dayOption = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        vm.formState = items[0];

        vm.changeFormState= changeFormState;
        vm.submitSackLunch = submitSackLunch;

        function changeFormState(){
            vm.formState = items[1];
        }
        function submitSackLunch(sackLunch){
            getOrCreateUserInformationService.getStudentInformation(studentID).then(successStudentInfo,errorSackLunch);

            function successStudentInfo(data){
                sackLunch[1] =  data.name;
                sackLunch[4] =  data.school;
                sackLunch[5] =  data.house;
                sackLunch[2] =   "Lunch";
                sackLunch[3] = sackLunch.week +' '+ sackLunch.day;
                delete sackLunch.day;
                delete sackLunch.week;
                if(sackLunch[vm.form.reoccurring] == false){
                    delete sackLunch[11];
                }
                console.log(sackLunch);
                var sackLunchData ={'sackLunch' : sackLunch};
                UserManager.requestLatePlate(sackLunchData).then(successSackLunch,errorSackLunch);

            }
        }

        function successSackLunch(data){
            console.log(data);
            if(data.status == 201){
                vm.serverMessage = "Sack Lunch Submitted Successfully.";
                vm.formState = items[2];
                $timeout(redirectToDashboard, 3000);

            }else{
                vm.serverMessage = data.response;
                vm.formState = items[2];
            }
        }
        function errorSackLunch(reason){
            console.log(reason);
            vm.serverMessage = reason;
            vm.formState = items[2];
        }

        function redirectToDashboard(){
            $location.path('/tab/dash');
        }
    }
})();