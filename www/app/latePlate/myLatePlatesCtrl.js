(function () {
    'use strict';

    angular
        .module('latePlate')
        .config(addRoute)
        .controller('myLatePlatesCtrl', myLatePlatesCtrl);


    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.my-late-plate', {
                url: '/my-latePlates',
                views: {
                    'tab-late-plate': {
                        templateUrl: 'app/latePlate/templates/my-latePlates.html',
                        controller: 'myLatePlatesCtrl',
                        resolve :{
                            myLatePlates:function(UserManager){
                                return UserManager.getUserLatePlates();

                            }
                        }
                    }
                }
            });
    }


    myLatePlatesCtrl.$inject = ['$scope', '$state','myLatePlates', '$ionicPopup', 'UserManager', 'alert',
        '$ionicScrollDelegate'];

    /* @ngInject */
    function myLatePlatesCtrl($scope, $state,myLatePlates, $ionicPopup, UserManager, alert,
                              $ionicScrollDelegate) {

        console.log('in myLatePlatesCtrl',myLatePlates);
        $scope.myLatePlates = myLatePlates;


        var errorFunction = function(){
            $ionicScrollDelegate.scrollTop();
            alert.createAlert('error',null,'Internal Server Error!');
        };

        var successLatePlateFunction = function(data){
            $scope.myLatePlates = data;
        };

        var successRemoveLatePlateFunction = function(data){
            console.log(data);
            if(data.hasOwnProperty('success')){
                if(data.success == 1){
                    alert.createAlert('success',null,'Late Plate Removed Successfully.');
                    //$state.go('tab.late-plate-main');
                    UserManager.getUserLatePlates().then(successLatePlateFunction,
                        errorFunction);
                }
            }else{
                $ionicScrollDelegate.scrollTop();
                alert.createAlert('error',null,'Internal Server Error!');
            }
        };

        // A confirm dialog
        $scope.cancelLatePlate = function(item) {
            var message = "Warning: Canceling A Reoccurring Late Plate will Terminate the Entire Group. " +
                "You will need to resubmit if you wish to continue repeating late plates after the one you cancel.";
            if(item.is_recurring != 1){
                var message = "Are you sure you want to cancel late plate for "+ item.formatted_date;
            }

            var confirmPopup = $ionicPopup.confirm({
                title: 'Cancel Late Plate',
                template: message,
                cancelText: 'Nevermind',
                cancelType: 'button-small button-light btn-nevermind',
                okText: 'Confirm',
                okType: 'button-small',
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    console.log(item);
                    UserManager.removeLatePlate(item.id).then(successRemoveLatePlateFunction,
                        errorFunction);

                } else {
                    console.log('You are not sure');
                }
            });
        };

    }
})();