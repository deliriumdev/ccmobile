(function () {
    'use strict';

    angular
        .module('latePlate', ['ngMessages','starter.services']);
})();