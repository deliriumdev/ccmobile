(function () {
    'use strict';

    angular
        .module('latePlate')
        .config(addRoute)
        .controller('latePlateCtrl', LatePlateCtrl);

    addRoute.$inject = ['$stateProvider'];
    /* @ngInject */
    function addRoute($stateProvider){
        $stateProvider
            .state('tab.late-plate', {
                url: '/latePlate',
                views: {
                    'tab-late-plate': {
                        templateUrl: 'app/latePlate/templates/latePlate-form-template.html',
                        controller: 'latePlateCtrl as vm',
                        resolve :{
                            studentID:function(UserInformationService)
                            {
                                return UserInformationService.getUserId();
                            }
                        }
                    }
                }
            });
    }

    LatePlateCtrl.$inject = ['UserManager','studentID','getOrCreateUserInformationService','$timeout','$location','MenuService','storageHelper'];

    /* @ngInject */
    function LatePlateCtrl(UserManager,studentID,getOrCreateUserInformationService,$timeout,$location,MenuService,storageHelper) {
        /* jshint validthis: true */
        var vm = this;
        var items = ['date', 'time', 'confirmation'];
        vm.title = 'Late Plate';
        vm.todayDate = moment().format("YYYY-MM-DD");
        vm.latePlate ={};
        vm.form = {
            terminationDate:'input_11',
            reoccurring:'input_10',
            date:'input_3',
            mealTime:'input_2',
            time:'input_7',
            message:'input_6'
        };
        vm.latePlate[vm.form.reoccurring]= false;
        vm.mealTime = ['Lunch','Dinner'];
        vm.weekOption = ['This Week','Next Week'];
        vm.dayOption = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        vm.formState = items[0];

        var dayFilter = function()
        {
            var dayToday = moment().format('dddd');
            console.log(dayToday);
            var index = vm.dayOption.indexOf(dayToday);
            switch (index) {
                case 0:
                    vm.dayOption = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
                    break;
                case 1:
                    vm.dayOption = ['Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
                    break;
                case 2:
                    vm.dayOption = ['Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
                    break;
                case 3:
                    vm.dayOption = ['Thursday', 'Friday', 'Saturday','Sunday'];
                    break;
                case 4:
                    vm.dayOption = ['Friday', 'Saturday','Sunday'];
                    break;
                case 5:
                    vm.dayOption = ['Saturday','Sunday'];
                    break;
                case 6:
                    vm.dayOption = ['Sunday'];
                    break;
            }
        }
        vm.filterDays = function()
        {
            if(vm.latePlate['week'] == vm.weekOption[0]) {
                dayFilter();
            }
            else
            {
                vm.dayOption = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
            }
        }

        dayFilter();
        if(storageHelper.get_isLateplate()){
            var data = MenuService.getMenu();
            vm.latePlate[vm.form.mealTime] = data[3]; //mealtime
            vm.latePlate.day = data[2]; // day
            vm.latePlate.week = data[35]; // date
            vm.formState = items[1];
            storageHelper.set_isLateplate(false);
        }


        vm.changeFormState= changeFormState;
        vm.submitLatePlate = submitLatePlate;

        function changeFormState(){
            vm.formState = items[1];
        }
        function submitLatePlate(latePlate){
            getOrCreateUserInformationService.getStudentInformation(studentID).then(successStudentInfo,errorLatePlate);

            function successStudentInfo(data){
                latePlate['student_id'] = studentID;
                latePlate['input_1'] =  data.name;
                latePlate['input_4'] =  data.school;
                latePlate['input_5'] =  data.house;
                if(moment(latePlate.week).isValid()){
                    latePlate['input_3'] = latePlate.week;
                }
                else
                {
                    if(latePlate.week == 'Next Week'){
                        latePlate['input_3'] = moment().day(latePlate.day).add(7, 'days').format('MM/DD/YYYY');
                    }
                    else{
                        latePlate['input_3'] = moment().day(latePlate.day).format('MM/DD/YYYY');
                    }
                }
                delete latePlate.day;
                delete latePlate.week;
                latePlate['input_10'] = vm.latePlate[vm.form.reoccurring];
                latePlate['input_6'] = vm.latePlate[vm.form.message];
                if(latePlate[vm.form.reoccurring] == false){
                    delete latePlate['input_11'];
                }
                else{
                    latePlate['input_11'] = moment(latePlate['input_11']).format('MM/DD/YYYY')
                }
                var latePlateData = {'input_values' : latePlate};
                UserManager.requestLatePlate(latePlateData).then(successLatePlate,errorLatePlate);
            }
        }

        function successLatePlate(data){
            if (data.status == 200){
                if(!data.response.is_valid){
                    var serverResponse = data.response.validation_messages;
                    //var errorMessage = null;
                    var errorMessage = [];
                    for (var fieldName in serverResponse) {
                        //var message = serverResponse[fieldName];
                        //errorMessage = errorMessage + message;
                        errorMessage.push(serverResponse[fieldName]);
                    }
                    vm.serverMessage = (0 == errorMessage.length) ? 'An unexpected error has occurred' : errorMessage.join(', ');
                    vm.formState = items[2];
                }
                else if (data.response.is_valid){
                    vm.serverMessage = "Late Plate Submitted Successfully.";
                    vm.formState = items[2];
//                    $timeout(redirectToDashboard, 3000);
                }
            }
            else {
                vm.serverMessage = "Unexpected Server Error!";
            }
        }
        function errorLatePlate(reason){
            console.log(reason);
            vm.serverMessage = reason;
            vm.formState = items[2];
        }

        function redirectToDashboard(){
            $location.path('/tab/dash');
        }

    }
})();
