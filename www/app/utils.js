/**
 * Created by imrantufail on 26/02/15.
 */
angular.module('starter.utils', [])

    .factory('SignatureFactory', function(REQUEST_KEYS) {
        var calculateSignature = function (route, method, expiration) {
            var d = new Date,
                unixtime = parseInt(d.getTime() / 1000),
                future_unixtime = unixtime + expiration,
                publicKey = "5ce98515ff",
                privateKey = "6498251fac801fc";
            stringToSign = publicKey + ":" + method + ":" + route + ":" + future_unixtime;

            var hash = CryptoJS.HmacSHA1(stringToSign, privateKey);
            var base64 = hash.toString(CryptoJS.enc.Base64);
            encodeURIComponent(base64);

            return {
                signature: encodeURIComponent(base64),
                expire: future_unixtime
            }
        }
        return {
            appendSignature: function (url,method, expiry)
            {
                var sig = calculateSignature(url, method, expiry);
                var signaturedUrl = url + '/?api_key=5ce98515ff&signature=' + sig.signature + '&expires=' + sig.expire;
                return signaturedUrl
            },
            appendToken: function(userId){
                var hash = CryptoJS.HmacSHA1(JSON.stringify({user:userId}), REQUEST_KEYS.secretKey);
                //var base64 = hash.toString(CryptoJS.enc.Base64);
                //return encodeURIComponent(base64);
                return encodeURIComponent(hash);
            }
        }
    })

    .factory('SearchFiltersFactory',function(){

        return{
            appendFieldFilters:function(url,data){
                var returnArray = [];
                var query = [];
                var obj;
                for (var k in data){
                    if (data.hasOwnProperty(k)) {
                        obj = {"key":k,"operator":"is","value":data[k]};
                        returnArray.push(obj);
                    }
                }

//                for (var f in returnArray){
//                    if (returnArray.hasOwnProperty(f)) {
//                        query.push('"'+ f + '":' + returnArray[f]);
//                    }
//                }

//                var argumentedUrl = url + '&search={"status":"active","field_filters":{' + query.join() + '}}';
//                var status = {"status":active}
                var queryParams = {"status":"active","field_filters":returnArray};
                return queryParams;
            }
        }

    })

    .factory('helperFunctionsFactory',function(){

        return{
            fillMenuTags:function(key,data){
                var tagArray = [];
                for(var i=0; i<3; i++){
                    key = key + .1;
                    if(data[key]){
                        tagArray.push(data[key]);
                    }
                }

                return tagArray;
            },
            CheckForType:function(key,data){
                var typeArray = [];
                for(var i=0; i<3; i++){
                    key = key + 0.1;
                    key = parseFloat(key.toFixed(1));
                    if(data[key]){
                        if(data[key].toUpperCase() === "GLUTEN FREE"){
                            typeArray[0] = true;
                        }
                        if(data[key].toUpperCase() === "VEGETARIAN"){
                            typeArray[1] = true;
                        }
                        if(data[key].toUpperCase() === "DAIRY FREE"){
                            typeArray[2] = true;
                        }
                    }
                }
                return typeArray;
            },
            schoolFilter:function(element) {
                return element.group_id > 1 && element.parent_id == null;
            },
            houseFilter:function(element) {
                return element.parent_id == this.group_id;
            }

        }
    });